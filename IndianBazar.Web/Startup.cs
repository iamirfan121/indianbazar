﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IndianBazar.Web.Startup))]
namespace IndianBazar.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
