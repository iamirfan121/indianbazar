﻿using IndianBazar.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndianBazar.Database
{
    public class CBContext : DbContext
    {

        public CBContext() : base("IndianBazarConnection")
        {

        }
        public DbSet<Product> Categories { get; set; }
        public DbSet<Category> Products { get; set; }

    }
}
